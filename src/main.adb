with Ada.Text_IO; --import
with Ada.Integer_Text_IO;

-- voyages quelque part: -20% sur 860e d'avion et 7j �  48e/j.
-- git push -f origin master
-- git add .
-- git commit -m "text"
-- git push

procedure Main is

   promo1 : integer := 20;
   prix : integer := 860;
   prix_jour : integer := 48;

   -- pr�ciser les range pour plus de robustesse
   type tpromo is digits 3 range 0.0 .. 100.0;
   type tprix is digits 6 range 0.0 .. 10_000.0;
   type tduree is range 0 .. 100;
   promo : tpromo := 20.0;
   trajet : tprix := 860.0;
   hotel : tprix := 48.0;
   duree : tduree := 7;
   duree_i : integer; --pour le Get
   total : tprix;
   -- type tpanete is (mercure, venus, terre, mars);
   -- pl : tplanete := venus;
   -- bl : boolean := true;
   type tjours is (lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche);

   type jsemaine is new tjours range lundi .. vendredi;
   subtype tgrandweekend is tjours range vendredi .. dimanche;
   subtype tgdweekend is tgrandweekend; --sans new. nouveau type identique (autre nom)
   --comme classe vide qui h�rite

   --test pour v�rifier qu'un subtype est compatible avec un type
   depart : tjours := samedi;
   arrivee : tgrandweekend := dimanche;
   voldenuit : boolean := depart /= arrivee;


   c1 : character := 'A';
   s1 : string := "okkk !";
   s2 : string := "Ceci est une chaine de caracteres";

   --compagnie : string (1..28);

   -- stockage dynamique dans une chaine: il faut pr�voir sa taille � l'avance
   infos : string := "Voyage en terre promise: "& promo'Image &"% sur "
     &trajet'Image&"euros de bateau a vapeur";
   --infostotal : string (1..28); --n�cessaire?

   i : integer := 2;
   k : integer := 1;

   type tdistance is array (0..100) of integer;
   type tdistance is array (intger range < >) of integer; --taille � d�finir

begin
   Ada.text_IO.Put_Line("Agence de voyages");
   Ada.text_IO.Put_Line("Nombre de jours:");
   Ada.Integer_Text_IO.Get(duree_i);
   duree := tduree(duree_i);

   Ada.text_IO.Put_Line("Voyage a Neverland: -"& promo1'Image &"% sur "
                        & prix'Image &" euros de bateau a vapeur et 7 jours a "
                        & prix_jour'Image& " par jour.");

   Ada.text_IO.Put_Line("Voyage a Neverland: -"& promo'Image &"% sur "
                        & trajet'Image &" euros de bateau a vapeur et "
                        & duree'Image & " jours a "
                        & hotel'Image& " par jour.");

   -- conversion interdite: total = hotel*dur�e
   -- calculer le total complet
   total := tprix(float(hotel) * float(duree) + float(trajet));
   total := total * tprix (1.0 - float(promo) / 100.0);

   Ada.text_IO.Put_Line("Prix total :"& total'Image);
   Ada.text_IO.Put_Line("Depart le :"& depart'Image);
   Ada.text_IO.Put_Line("Depart dimanche? "& boolean(depart=dimanche)'Image);

   Ada.text_IO.Put_Line("Voici s2 : "& s2);
   Ada.text_IO.Put_Line("Infos : "& infos);
   Ada.text_IO.Put_Line("Vol de nuit : "& voldenuit'Image);


   case duree is
      when 0..7 => Ada.text_IO.Put_Line("Compagnie = Air Canada");
      when 14 | 21 | 28 => Ada.text_IO.Put_Line("Compagnie = Quebec'air");
      when others => Ada.text_IO.Put_Line("Compagnie = Air Canada");
   end case;

   Ada.text_IO.Put_Line(if duree >45 then "Attention au visa!" else "");
   --if dans Put_Line: else obligatoire

   --Avion au d�but et � la fin. Trois jours de marche et un jour de repos. Afficher programme s�jour.

   -- i=1
   --Ada.text_IO.Put_Line("Jour 1: avion");
   --while i <= integer(duree) loop
      --k := 1;
      --while k <= 3 loop
      --Ada.text_IO.Put_Line("Jour"&i'Image&": marche"); --erreur: il faut arr�ter quand i = duree
         --i := i + 1;
         --k := k + 1;
      --end loop;
      --Ada.text_IO.Put_Line("Jour"&i'Image&": repos");
      --i := i + 1;
   --end loop;
      --Ada.text_IO.Put_Line("Jour "&duree'Image&": avion");

   for i in 1..duree loop
      if i = 1 or i  = duree then
         Ada.text_IO.Put_Line("Jour"&i'Image&": avion");
      elsif i mod 4 = 1 then
         Ada.text_IO.Put_Line("Jour"&i'Image&": repos");
      else
         Ada.text_IO.Put_Line("Jour"&i'Image&": marche");
      end if;
   end loop;







end Main;
