with Ada.Text_IO; --import

procedure Main is

   --pragma = info pour le compilateur
   --pragma Optimize(Off);
   ava : integer := 3;
   b : integer := integer'first; -- 'first = entier le plus petit possilbe
   c : integer := integer'last; --integer cod� sur 32 bits = 4 octets

   type tb is range 0 .. 1_000_000; -- _ facultatif (pour la lisibilit�)
   --tb est situ� entre 0 est 1_000000 (inclus), donc 1_000_000+1 valeurs possibles
   d : tb;
   type modulo is mod 1_000_0001;
   eff : modulo;
   k : float;

   aa : integer := -7 mod (-4);
   bb : integer := -7 mod (-4);

begin
   Ada.text_IO.Put_Line("A"); --Put_Line (retour � la ligne) Put (pas retour ligne)
   Ada.text_IO.Put_Line("*");
   ava := ava + 5;
   Ada.Text_IO.Put_Line(ava'Image); --'Image convetit en text
   Ada.Text_IO.Put_Line("b'image = " & b'Image);
   Ada.Text_IO.Put_Line(c'Image);

   eff := eff+2;
   Ada.Text_IO.Put_Line("eff+2= " & eff'Image);

   --a := tb'last - tb'first;

   d := 999_999;
   --d := d+2; --on d�passe la limite tb... CONSTRAINT_ERROR

   Ada.Text_IO.Put_Line("(mod) aa ="&aa);
   Ada.Text_IO.Put_Line("(rem) bb ="&bb;

end Main;
